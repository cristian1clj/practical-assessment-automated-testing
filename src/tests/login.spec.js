const { pages } = require('./../po');

describe('login page tests', () => {
  beforeEach(async () => {
    await pages('login').open();
  });

  it('UC -1 - Test Login form with empty credentials', async () => {
    // Type any credentials
    await pages('login').setLoginValues('AnyUsername', 'AnyPassword');
    // Clear the inputs
    await pages('login').loginWrapper.usernameInput.clearValue();
    await pages('login').loginWrapper.passwordInput.clearValue();
    // Check the error messages: 3.1 Username is required
    await pages('login').loginWrapper.loginBtn.click();
    expect(await pages('login').loginWrapper.errorMessage.getValue())
    .toHaveTextContaining('username is required', {ignoreCase: true});
  });

  it('UC -2 - Test Login form with credentials by passing Username', async () => {
    // Type any credentials in username and password
    await pages('login').setLoginValues('AnyUsername', 'AnyPassword');
    // Clear the password input
    await pages('login').loginWrapper.passwordInput.clearValue();
    // Check the error messages: 3.1 Password is required
    await pages('login').loginWrapper.loginBtn.click();
    expect(await pages('login').loginWrapper.errorMessage.getValue())
    .toHaveTextContaining('password is required', {ignoreCase: true});
  });

  it('UC -3 - Test Login form with credentials by passing Username & Password', async () => {
    // Type credentials in username which are under Accepted username are sections
    // Enter password as secret_sauce.
    await pages('login').setLoginValues('standard_user', 'secret_sauce');
    // Click on Login and validate the title “Swag Labs” in the dashboard
    await pages('login').loginWrapper.loginBtn.click();
    expect(await browser.getTitle()).toEqual('Swag Labs');
    expect(await pages('dashboard').topbar.title).toHaveText('Swag Labs');
  });
});