const BasePage = require('./base.page');

const { TopbarComponent } = require('../components');

class DashboardPage extends BasePage {

  constructor() {
    super('inventory.html');
    this.topbar = new TopbarComponent();
  }

}

module.exports = DashboardPage;