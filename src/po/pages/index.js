const LoginPage = require('./login.page');
const DashboardPage = require('./dashboard.page');

/**
 * 
 * @param {'login' | 'dashboard'} name 
 * @returns {LoginPage | DashboardPage}
 */
function pages(name) {
  const items = {
    login: new LoginPage(),
    dashboard: new DashboardPage()
  }
  return items[name.toLowerCase()];
}

module.exports = {
  LoginPage,
  DashboardPage,
  pages
}