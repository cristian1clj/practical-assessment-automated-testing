const BasePage = require('./base.page');

const { LoginWrapperComponent } = require('../components');

class LoginPage extends BasePage {

  constructor() {
    super('');
    this.loginWrapper = new LoginWrapperComponent();
  }

  async setLoginValues(username, password) {
    await this.loginWrapper.usernameInput.setValue(username);
    await this.loginWrapper.passwordInput.setValue(password);
  }

}

module.exports = LoginPage;