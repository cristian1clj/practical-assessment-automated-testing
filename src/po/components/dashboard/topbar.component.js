const BaseComponent = require('./../common/base.component');

class TopbarComponent extends BaseComponent {

  constructor() {
    super('#header_container');
  }

  get title() {
    return this.rootEl.$('.app_logo');
  }

}

module.exports = TopbarComponent;