const BaseComponent = require('../common/base.component');

class LoginWrapperComponent extends BaseComponent {

  constructor() {
    super('.login_wrapper-inner');
  }

  get usernameInput() {
    return this.rootEl.$('#user-name');
  }

  get passwordInput() {
    return this.rootEl.$('#password');
  }

  get loginBtn() {
    return this.rootEl.$('#login-button');
  }

  get errorMessage() {
    return this.rootEl.$('//h3[@data-test="error"]');
  }

}

module.exports = LoginWrapperComponent;