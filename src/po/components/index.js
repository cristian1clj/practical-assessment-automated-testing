const LoginWrapperComponent = require('./login/loginWrapper.component');

const TopbarComponent = require('./dashboard/topbar.component');

module.exports = {
  LoginWrapperComponent,
  TopbarComponent
}