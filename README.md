# practical-assessment-automated-testing



This project is aimed at conducting various tests using WebDriverIO on the `https://www.saucedemo.com/` website.

## How to Run the Tests

To run the tests, follow these steps:

1. Clone this repository to your local machine.

```bash
git clone https://gitlab.com/cristian1clj/practical-assessment-automated-testing.git
```


2. Navigate to the project directory.

```bash
cd practical-assessment-automated-testing
```


3. Install the required dependencies using npm.

```bash
npm install
```


4. Run tests with the following command.

```bash
npm test
```
